CPP=g++

OBJECTS=utilities.o
		
BINS=get_runs
		
CPPFLAGS=-std=c++11 -O3 -DNDEBUG -I ~/include -L ~/lib -lsdsl -ldivsufsort -ldivsufsort64
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

get_runs:
	$(CPP)  -o $(DEST)/get_runs get_runs.cpp $(OBJECTS) $(CPPFLAGS)

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
