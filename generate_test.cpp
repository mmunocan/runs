#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char ** argv){
	
	ofstream raster("bin1.bin");
	for(int i = 0; i < 16; i++){
		raster.write((char *) (& i), sizeof(int));
	}
	raster.close();
	
	ofstream raster2("bin2.bin");
	for(int i = 16; i < 32; i++){
		raster2.write((char *) (& i), sizeof(int));
	}
	raster2.close();
	
	return 0;
}