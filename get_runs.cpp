/*
 * Entropia de celdas en orden cero manual en row major orden
 */
#include <fstream>
#include <iostream>
#include <vector>
#include <iomanip>
#include <sdsl/vectors.hpp>
#include <sdsl/bit_vectors.hpp>

#include "utilities.hpp"

using namespace std;
using namespace sdsl;

void get_runs(const vector<int> & datas, vector<int> & vals, vector<size_t> & reps){
	size_t n = datas.size();
	
	// Inicializacion
	int last = datas[0];
	vals.push_back(datas[0]);
	reps.push_back(0);
	
	for(size_t i = 1; i < n; i++){
		if(datas[i] != last){
			// El inicio de un nuevo run
			last = datas[i];
			vals.push_back(datas[i]);
			reps.push_back(i);
		}
	}
	
	
}

int main(int argc, char ** argv){
	if(argc != 4){
		cout << "usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters>" << endl;
		return -1;
	}
	/******************************************/
	/* Recepción de los parámetros de entrada */
	/******************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	
	vector<int> datas;
	read_and_linearize(datas, inputs_file, inputs_path, n_elements);
	
	vector<int> vals;
	vector<size_t> reps;
	get_runs(datas, vals, reps);
	
	size_t n = datas.size();
	size_t r = vals.size();
	
	int_vector<> vals_c(r);
	bit_vector reps_c = bit_vector(n, 0);
	
	for(size_t i = 0; i < r; i++){
		vals_c[i] = vals[i]>>1;
		reps_c[reps[i]] = 1;
	}
	
	rank_support_v<1> reps_rank(&reps_c);
	
	cout << r << "   " << fixed << setprecision(2) << size_in_mega_bytes(vals_c) << "MB   " << size_in_mega_bytes(reps_c) << "MB   " << size_in_mega_bytes(reps_rank) << "MB" << endl;
	
	
	return 0;
}