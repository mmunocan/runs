#!/usr/bin/bash

PATH="../k2raster-2022/dataset/temporal/Temporal/NASA/NLDAS_FORA0125_H/"
RASTERS=2664

./get_runs $PATH"APCP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"CAPE_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"CONVfrac_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"DLWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"DSWRF_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"PEVAP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"PRES_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"SPFH_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"TMP_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"UGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS
./get_runs $PATH"VGRD_NLDAS_FORA0125_H.txt" $PATH $RASTERS