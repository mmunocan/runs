#include "utilities.hpp"

unsigned long interleave_uint32_with_zeros(unsigned int input)  {
    unsigned long word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
    return word;
}

unsigned long get_zorder(unsigned int n_rows, unsigned int n_cols){
	return interleave_uint32_with_zeros(n_cols) | (interleave_uint32_with_zeros(n_rows) << 1);
}

unsigned int get_k(unsigned int n_rows, unsigned int n_cols){
	if(n_rows >= n_cols){
		return  (1 << 32-__builtin_clz(n_rows-1));
	}else{
		return  (1 << 32-__builtin_clz(n_cols-1));
	}
}

bool read_and_linearize(vector<int> & datas, string inputs_file, string inputs_path, size_t n_elements){
	// Permite leer los archivos de entrada sin errores
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	string raster_filename;
	int value;
	size_t position;
	
	/**********************************/
	/* Lectura del indíce de imagenes */
	/**********************************/
	ifstream input_global(inputs_file);
	if(!input_global.is_open()){
		cout <<  inputs_file << " can not be opened" << endl;
		return false;
	}
	input_global >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	getline(input_global, raster_filename); // Salto de linea inicial (vacio)
	
	size_t side = get_k(n_rows, n_cols);
	size_t double_side = side * side;
	
	datas.resize(n_elements * double_side, -1);
	
	/************************/
	/* Procesar cada raster */
	/************************/
	for(size_t rast = 0; rast < n_elements; rast++){
		
		/*******************************************************/
		/* Obtener el nombre del archivo del raster particular */
		/*******************************************************/
		
		getline(input_global, raster_filename);
		ifstream input_raster(inputs_path+raster_filename);
		if(!input_raster.is_open()){
			cout << inputs_path << raster_filename << " can not be opened" << endl;
			return false;
		}
		
		/*****************************************/
		/* Procesar cada celda dentro del raster */
		/*****************************************/
		for(size_t row = 0; row < n_rows; row++){
			for(size_t col = 0; col < n_cols; col++){
				input_raster.read((char *) (& value), sizeof(int));
				
				position = get_zorder(row, col) * n_elements + rast;
				
				datas[position] = value;
			}
			
		}
		input_raster.close();
		
	}
	
	input_global.close();
	
	return true;
}

